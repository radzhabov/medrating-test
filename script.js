let usersUrl = 'https://json.medrating.org/users',
    albumUrl = 'https://json.medrating.org/albums';

let gallery = document.querySelector('.gallery'),
    contentBlock = document.querySelector('.content'),
    catalogBlock = document.querySelector('.catalog'),
    favouritesBlock = document.querySelector('.favourites'),
    catalogButton = document.querySelector('#catalog'),
    favouritesButton = document.querySelector('#favourites');

// Проверяем, есть ли в localStorage добавленные элементы
let itemsArray = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];
localStorage.setItem('items', JSON.stringify(itemsArray));
const data = JSON.parse(localStorage.getItem('items'));

// Загружаем пользователей
fetch(usersUrl)
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    data.forEach(item => {
      if (item.name) {
        catalogBlock.insertAdjacentHTML('beforeend', `<div class="user" id="user${item.id}"><p class="name">${item.name}</p></div>`)
      }
    })
    loadAlbums()
  })
  .catch(function (err) {
    console.log('Error: ' + err.message);
  });

// Загружаем альбомы при помощи async await
async function loadAlbums() {
  let response = await fetch(albumUrl);
  let data = await response.json();

  data.forEach(item => {
    if (item.userId) {
      document.querySelector(`#user${item.userId}`).insertAdjacentHTML('beforeend', `<div class="album" album-id="${item.id}"><p class="name" onclick="loadPhotos(this)">${item.title}</p></div>`);
    }
  })
}

// Реализация загрузки фотографий
function loadPhotos(albumName) {
  let albumBlock = albumName.parentElement,
      albumBlockId = albumBlock.getAttribute('album-id');

  // Обращаемся только к определенному альбому
  let photosUrl = `https://json.medrating.org/photos?albumId=${albumBlockId}`;

  fetch(photosUrl)
    .then(function (response) {
      return response.json()
    })
    .then(function (data) {
      data.forEach(item => {
        albumBlock.insertAdjacentHTML('beforeend', `<div class="image-block" image-id="${item.id}"><div class="image-block__inner-block"><img src="${item.thumbnailUrl}" big-src="${item.url}" class="image" title="${item.title}"><img src="star.png" class="star star-default" onclick="addFavourites(this)" image-id="${item.id}"><img src="star-black.png" class="star star-active" onclick="removeFavourites(this)" image-id="${item.id}"></div></div>`)

        // Проверяем, добавлялась ли данная фотография в избранное
        if(itemsArray.includes(item.id.toString())) {
          let imageBlock = document.querySelector(`.image-block[image-id="${item.id}"]`);
          imageBlock.querySelector('.image-block__inner-block').classList.add('added');
        }
      })
    })

  albumName.removeAttribute('onclick');
}

// Добавление в избранное
function addFavourites(star) {
  star.parentElement.classList.add('added');

  let imageId = star.getAttribute('image-id');
  
  itemsArray.push(imageId);
  localStorage.setItem('items', JSON.stringify(itemsArray.sort((a,b) => a - b )))
}

// Удаление из избранного
function removeFavourites(star) {
  star.parentElement.classList.remove('added');

  let imageId = star.getAttribute('image-id');

  itemsArray = itemsArray.filter((n) => { return n != imageId });
  localStorage.setItem('items', JSON.stringify(itemsArray.sort((a, b) => a - b)));

  let imageBlock = document.querySelector(`.image-block[image-id="${imageId}"]`);
  imageBlock.querySelector('.image-block__inner-block').classList.remove('added');

  favouritesBlock.innerHTML = '';

  if(itemsArray.length > 0) {
    itemsArray.forEach(item => {
      let photoUrl = `https://json.medrating.org/photos?id=${item}`;

      fetch(photoUrl)
        .then(function (response) {
          return response.json()
        })
        .then(function (data) {
          data.forEach(item => {
            favouritesBlock.insertAdjacentHTML('beforeend', `<div class="image-block" image-id="${item.id}"><p>${item.title}</p><div class="image-block__inner-block added"><img src="${item.thumbnailUrl}" big-src="${item.url}" class="image" title="${item.title}"><img src="star.png" class="star star-default" onclick="addFavourites(this)" image-id="${item.id}"><img src="star-black.png" class="star star-active" onclick="removeFavourites(this)" image-id="${item.id}"></div></div>`)
          });
        })
    })
  } else {
    favouritesBlock.insertAdjacentHTML('afterbegin', `<p>В избранном ничего нет</p>`)
  }
}

// Открытие вкладки "Каталог"
function openCatalog() {
  favouritesBlock.classList.remove('active');
  catalogBlock.classList.add('active');

  favouritesBlock.innerHTML = '';
}

// Открытие вкладки "Избранное"
function openFavourites() {
  if(favouritesBlock.classList.contains('active')) {
    return false
  }
  
  catalogBlock.classList.remove('active');
  favouritesBlock.classList.add('active');

  if (itemsArray.length > 0) {
    itemsArray.forEach(item => {
      let photoUrl = `https://json.medrating.org/photos?id=${item}`;

      fetch(photoUrl)
        .then(function (response) {
          return response.json()
        })
        .then(function (data) {
          data.forEach(item => {
            favouritesBlock.insertAdjacentHTML('beforeend', `<div class="image-block" image-id="${item.id}"><p>${item.title}</p><div class="image-block__inner-block added"><img src="${item.thumbnailUrl}" big-src="${item.url}" class="image" title="${item.title}"><img src="star.png" class="star star-default" onclick="addFavourites(this)" image-id="${item.id}"><img src="star-black.png" class="star star-active" onclick="removeFavourites(this)" image-id="${item.id}"></div></div>`)
          });
        })
    })
  } else {
    favouritesBlock.insertAdjacentHTML('afterbegin', `<p>В избранном ничего нет</p>`)
  }
}

// Реализуем открытие и закрытие вкладок в каталоге
function toggleBlocks(event){
  let element = event.target;
  if (element.classList.contains('name')) {
    if (element.parentElement.classList.contains('open')) {
      element.parentElement.classList.remove('open')
    } else {
      element.parentElement.classList.add('open')
    }
  }
}

// Закрываем картинку в полный размер
function closeGallery() {
  gallery.innerHTML = '';
  gallery.classList.remove('open');
}

// Открывает картинку в полный размер
function openGallery(event) {
  let element = event.target;
  if (element.classList.contains('image')) {
    gallery.classList.add('open');
    gallery.insertAdjacentHTML('beforeend', `<img src="${element.getAttribute('big-src')}" title="${element.getAttribute('title')}">`)
  }
}

catalogButton.addEventListener('click', openCatalog);

favouritesButton.addEventListener('click', openFavourites);

contentBlock.addEventListener('click', toggleBlocks);

gallery.addEventListener('click', closeGallery);

contentBlock.addEventListener('click', openGallery);
